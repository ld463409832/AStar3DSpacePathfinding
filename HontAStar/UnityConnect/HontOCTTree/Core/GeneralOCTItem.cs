﻿using UnityEngine;
using System.Collections;

namespace Hont
{
    public class GeneralOCTItem<T> : IOCTItem<T>
    {
        public Vector3 Position { get; set; }
        public T Value { get; set; }
    }
}
