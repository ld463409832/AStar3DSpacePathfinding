﻿using UnityEngine;
using System.Collections;

namespace Hont
{
    public interface IOCTNodeSyncer<T>
    {
        void Sync(OCTNode<T> node);
    }
}
